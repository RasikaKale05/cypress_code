/// <reference types="cypress" />

describe('My First Test', function()
{
    it('Verify a Website', function() 
    {
        cy.visit('https://dev-cbt.dilipoakacademy.com/#/login') // open website
        cy.get('#mat-input-0').should('be.visible').should('be.enabled').type("4444444444") // enter id
        cy.get('#mat-input-1').should('be.visible').should('be.enabled').type("password") // enter password
        cy.get(".mat-primary[type='button']").click()// click on login
        cy.wait(5000)
        cy.get("ul > :nth-child(3)").click()// click on Question Bank
        cy.get(".mat-primary[type='button']").click()// click on Add Question
        cy.get("#mat-input-2").should('be.visible').type("12345")//enter reference id
        cy.get("#mat-select-1 > .mat-select-trigger > .mat-select-arrow-wrapper").click()//Difficulty level
        cy.get("#mat-option-7").should('be.visible').click()
        cy.get("#mat-select-2 > .mat-select-trigger > .mat-select-arrow-wrapper").click()//Question for
        cy.get("#mat-option-10").should('be.visible').click()
        cy.get("#mat-select-3 > .mat-select-trigger > .mat-select-arrow-wrapper").click()//Section
        cy.get("#mat-option-14 ").should('be.visible').click()
        cy.get("#mat-select-4 > .mat-select-trigger > .mat-select-arrow-wrapper").click()//Category
        cy.get("#mat-option-17").should('be.visible').click()
        cy.wait(5000)
        cy.get("#mat-select-7 > .mat-select-trigger > .mat-select-arrow-wrapper").click()//Tag
        cy.get("#mat-option-20").click()
        cy.get("#mat-option-25").click()
        cy.wait(5000)
        cy.get("[title='Next'] > .mat-raised-button").click({force: true})// Next button
        cy.get("#mat-radio-3 > .mat-radio-label > .mat-radio-container > .mat-radio-outer-circle").should('be.visible').should('not.be.checked').click({force: true})//Redio button
        // programmatically upload the logo
            const image = 'Question.png';
            cy.get('input[type=file]').attachFile(image)
         cy.get('.dialog-error-action > app-iro-button > .mat-raised-button').click()
         cy.get('#mat-select-8 > .mat-select-trigger > .mat-select-arrow-wrapper > .mat-select-arrow').click()
         cy.wait(5000)
         cy.get('#mat-option-72 > .mat-option-text').click()
         const image1 = 'Question.png';
         cy.get('[fxflex="75"] > input[type=file]').attachFile(image1)
         cy.get('.dialog-error-action > app-iro-button > .mat-raised-button').click()
         cy.wait(1000)
         cy.get('#mat-input-4').should('be.visible').should('be.enabled').type("4")         
         cy.wait(1000)
         cy.get('#mat-input-5').should('be.visible').should('be.enabled').type("5")
         cy.get('#mat-checkbox-2').click()
         cy.wait(5000)
         cy.get('iframe#mce_1_ifr').then(function($iframe){
           const iframecontent = $iframe.contents().find('body')
           cy.wrap(iframecontent).type('No Justification')
        })
         cy.get('[title="Next"] > .mat-raised-button').click()
         cy.get('[colored="accent"] > .mat-raised-button > .mat-button-wrapper').click()
        })
})