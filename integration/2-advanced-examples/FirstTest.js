describe('My First Test', function()
{
    it('Verify title of the website', function() 
    {
      cy.visit('https://dev-cbt.dilipoakacademy.com/#/login')
      cy.title().should('eq',"Dilip Oak's Academy")
      cy.url().should('include','dilipoakacademy')//verify URL should include "dilipoakacademy"
      cy.get('#mat-input-0').should('be.visible').should('be.enabled').type("4444444444") // enter id
      cy.get('#mat-input-1').should('be.visible').should('be.enabled').type("password") // enter password
      cy.get(".mat-primary[type='button']").click()
      cy.wait(5000)
      cy.get('p.ng-star-inserted').should('contain.text','Dashboard')
      cy.get("ul > :nth-child(3)").click()
      cy.go('back')
      cy.get('p.ng-star-inserted').should('contain.text','Dashboard')
      cy.go('forward')
      
    })
  })