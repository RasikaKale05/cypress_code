
/// <reference types="cypress" />

describe('website testing', function()
{
    before(function(){
        cy.fixture('User_info').then(function(data){
            this.data = data
        })
    })
    it('Verify a Website', function() 
    {
        cy.visit('http://3.81.251.227/#/home/user-info') // open website (Users_Info)
        cy.get('#mat-input-0').should('be.visible').should('be.enabled').type(this.data.Password) // enter password
        cy.get('#mat-input-1').should('be.visible').should('be.enabled').type(this.data.CPassword) // enter confirm password
        cy.get('app-button[name="Set Password"]').should('be.visible').click()// click on set password button
        // Verify Personal details
        cy.get('#mat-input-4').should('be.visible').should('be.enabled').type(this.data.Name)// enter first name
        cy.get('#mat-input-5').should('be.visible').should('be.enabled').type(this.data.Middle)// enter middle name
        cy.get('#mat-input-6').should('be.visible').should('be.enabled').type(this.data.Last)// enter middle name
        cy.get('#mat-input-2').should('be.enabled').should('be.visible').click()// Click on Date of Birth
        // select Date of Birth from calendar
        cy.get('#mat-calendar-button-0').click()
        cy.get('td.ng-star-inserted').contains(this.data.Year).click()
        cy.get('td.ng-star-inserted').contains(this.data.Month).click()
        cy.get('td.ng-star-inserted').contains(this.data.Day).click()
        cy.get('#mat-input-7').should('be.visible').should('be.enabled').type(this.data.Email)// Enter email id
        cy.get('#mat-input-8').should('be.visible').should('be.enabled').type(this.data.Phone_Number1)// enter mobile number
        cy.get('.mat-radio-label-content').contains(this.data.Gender).click()//select gender Radio
        cy.get('#mat-select-0').click()
        cy.get('.mat-option-text').contains(this.data.Job_Position).click()//select job Position
        cy.get('#mat-input-9').should('be.visible').should('be.enabled').type(this.data.Current_Address)//Enter Current address
        cy.get('#mat-input-10').should('be.visible').should('be.enabled').type(this.data.City)// Enter city
        cy.get('#mat-input-11').should('be.visible').should('be.enabled').type(this.data.Pincode)// Enter pincode
        // if user has current address and permanent address is same
        if(this.data.Current_Address==this.data.Parmanent_Address)
        cy.get('.mat-checkbox-inner-container').click()
        else
        cy.get('#mat-input-12').should('be.enabled').type(this.data.Parmanent_Address)// if user has current address and permanent address is different
        
        const image1 = this.data.Attach_file;
        cy.get('input[type="file"]').attachFile(image1)
        cy.get('#mat-input-3').should('be.visible').should('be.enabled').type(this.data.About_Yourself)// About your self
        cy.get('#mat-input-13').should('be.visible').should('be.enabled').type(this.data.Aadhar_Number)// enter Aadhar number
        cy.get('#mat-input-14').should('be.visible').should('be.enabled').type(this.data.Pan_Number)// Enter pan Number
        cy.get('#mat-input-15').should('be.visible').should('be.enabled').type(this.data.Passport)// enter Passport number
        cy.get('#mat-input-16').should('be.visible').should('be.enabled').type(this.data.UAN)// enter UAN number
        cy.get('#mat-input-17').should('be.visible').should('be.enabled').type(this.data.Bank_Name)// enter bank name
        cy.get('#mat-input-18').should('be.visible').should('be.enabled').type(this.data.IFSC_Code)// Enter IFSC Code
        cy.get('#mat-input-19').should('be.visible').should('be.enabled').type(this.data.Account_Number)// Enter Account number
        cy.get('#mat-input-20').should('be.visible').should('be.enabled').type(this.data.Emergency_contact_Person)// enter Emergency contact person
        cy.get('#mat-select-2').click()// select Relation
        cy.get('.mat-option-text').contains(this.data.Relation).click()
        cy.get('#mat-input-21').should('be.visible').should('be.enabled').type(this.data.Phone_Number2)// enter Phone Number
        cy.get('#mat-input-22').should('be.visible').should('be.enabled').type(this.data.Reference_employee)// enter reference employee name(optional)
        cy.get('#mat-input-23').should('be.visible').should('be.enabled').type(this.data.Employee_id)// enter employee email id(optional)
        //cy.get('app-button[name="Next"]').should('be.visible').should('be.enabled').click()// click on Next button
        cy.get(':nth-child(2) > [fxlayout="column"] > :nth-child(1) > .circle > .step-inactive').click()
        cy.get('#mat-input-24').should('be.visible').should('be.enabled').type(this.data.Mother_Name)// enter Mother name 
        cy.get('#mat-input-25').should('be.visible').should('be.enabled').type(this.data.Father_name)// enter Father name
        //if marital status is Unmarried
        if(this.data.Marital_Status == "Unmarried")
        cy.get('.mat-radio-label-content').contains('Unmarried').click()// select Marital Status
        else
        {
        cy.get('.mat-radio-label-content').contains('Married').click()// if Married
        cy.get('.mat-checkbox-inner-container').click()// Mark as confidencial
        cy.get('#mat-input-28').should('be.visible').should('be.enabled').type(this.data.Spouse_Name)//Enter Spouse Name
       //Date of Birth
        cy.get('#mat-input-26').click()
        cy.get('.mat-calendar-period-button').click()
        cy.get('td.ng-star-inserted').contains(this.data.SYear).click()
        cy.get('td.ng-star-inserted').contains(this.data.SMonth).click()
        cy.get('td.ng-star-inserted').contains(this.data.SDay).click()
        cy.get('.mat-radio-label').contains(this.data.SGender).click()//select gender Radio
        cy.get('#mat-input-27').click()
        //Select Anniversary date
        cy.get('.mat-calendar-period-button').click()
        cy.get('td.ng-star-inserted').contains(this.data.A_Year).click()
        cy.get('td.ng-star-inserted').contains(this.data.A_Month).click()
        cy.get('td.ng-star-inserted').contains(this.data.A_Day).click()
        cy.get('#mat-input-29').should('be.enabled').type(this.data.Child1)//Enter Child name
         if(this.data.CGender=='Male')
         cy.get('#mat-radio-11').contains(this.data.CGender).click()// select child gender from radio button
        else 
         {
            cy.get('#mat-radio-12 ').contains(this.data.CGender).click()// select child gender from radio button
         }
        // More if child
         if(this.data.No_Child>1)
        cy.get('app-button[name="Add"]').click()
        }
        //Click on next button
        //cy.get('[name="Next"] > .mat-focus-indicator').click()
        cy.get(':nth-child(3) > [fxlayout="column"] > :nth-child(1) > .circle > .step-inactive').click()//Selct Educational Details
        cy.get('#mat-input-28').should('be.enabled').type(this.data.Qualification)// Enter Qualification
        cy.get('#mat-input-29').should('be.enabled').type(this.data.Institute)// Enter Institute
        cy.get('#mat-input-30').should('be.enabled').type(this.data.University)// Enter University
        cy.get('#mat-input-31').should('be.enabled').type(this.data.GAP)// Enter GAP
        //Select Start Year
        cy.get('#mat-input-26').click()
        cy.get('.mat-calendar-period-button').click()
        cy.get('td.ng-star-inserted').contains(this.data.Start_Year).click()
        cy.get('td.ng-star-inserted').contains(this.data.Start_Month).click()
        cy.get('td.ng-star-inserted').contains(this.data.Start_Day).click()
        //Select End Year
        cy.get('#mat-input-27').click()
        cy.get('.mat-calendar-period-button').click()
        cy.get('td.ng-star-inserted').contains(this.data.End_Year).click()
        cy.get('td.ng-star-inserted').contains(this.data.End_Month).click()
        cy.get('td.ng-star-inserted').contains(this.data.End_Day).click()
        // Attach Certificate
        const image2 = 'demo_Certificate.jpg';
        cy.get('input[type="file"]').attachFile(image2)
        for(let i = 1; i < this.data.No_Q; i++ ){
        cy.get(':nth-child(5) > app-button > .mat-focus-indicator').click()// click on Add button
        cy.get('#mat-input-34').should('be.enabled').type(this.data.Qualification2)// Enter Qualification
        cy.get('#mat-input-35').should('be.enabled').type(this.data.Institute2)// Enter Institute
        cy.get('#mat-input-36').should('be.enabled').type(this.data.University2)// Enter University
        cy.get('#mat-input-37').should('be.enabled').type(this.data.GAP2)// Enter GAP
        //Select Start Year
        cy.get('#mat-input-32').click()
        cy.get('.mat-calendar-period-button').click()
        cy.get('td.ng-star-inserted').contains(this.data.Start_Year2).click()
        cy.get('td.ng-star-inserted').contains(this.data.Start_Month2).click()
        cy.get('td.ng-star-inserted').contains(this.data.Start_Day2).click()
        //Select End Year
        cy.get('#mat-input-33').click()
        cy.get('.mat-calendar-period-button').click()
        cy.get('td.ng-star-inserted').contains(this.data.End_Year2).click()
        cy.get('td.ng-star-inserted').contains(this.data.End_Month2).click()
        cy.get('td.ng-star-inserted').contains(this.data.End_Day2).click()
        // Attach Certificate
        const image3 = this.data.Attach_Certificate2;
        cy.get('input[type="file"]').attachFile(image3)
             }
        // Enter Work Details
        cy.get(':nth-child(4) > [fxlayout="column"] > :nth-child(1) > .circle > .step-inactive').click()
         //Self Declaration
         cy.get('.mat-checkbox-inner-container').click()
        // Select Work Experience
        cy.get('.ng-star-inserted').contains(this.data.Work).click()
        cy.get('#mat-input-40').should('be.enabled').type(this.data.Last_Designation)
        cy.get('#mat-input-41').should('be.enabled').type(this.data.Company_name)
        cy.get('#mat-input-42').should('be.enabled').type(this.data.Company_Email)
        cy.get('#mat-input-43').should('be.enabled').type(this.data.Company_Phone)
        cy.get('#mat-input-38').click()
        cy.get('.mat-calendar-period-button').click()
        cy.get('td.ng-star-inserted').contains(this.data.EStart_Year).click()
        cy.get('td.ng-star-inserted').contains(this.data.EStart_Month).click()
        cy.get('td.ng-star-inserted').contains(this.data.EStart_Day).click()
        //Select End Year
        cy.get('#mat-input-39').click()
        cy.get('.mat-calendar-period-button').click()
        cy.get('td.ng-star-inserted').contains(this.data.EEnd_Year).click()
        cy.get('td.ng-star-inserted').contains(this.data.EEnd_Month).click()
        cy.get('td.ng-star-inserted').contains(this.data.EEnd_Day).click()
        const image4 = this.data.Attach_Certificate2;
        cy.get('input[type="file"]').attachFile(image4)
        //Self Declaration
        cy.get('.mat-checkbox-inner-container').click()
        //Submit button
        //cy.get('app-button[name="Submit"]').click()
        })
})